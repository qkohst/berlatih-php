<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>tukar-besar-kecil (KUKOH SANTOSO)</title>
</head>

<body>
  <?php
  function tukar_besar_kecil($string)
  {
    $a = "Hello World";
    $b = "I aM aLAY";
    $c = "My Name is Bond!!";
    $d = "IT sHOULD bE me";
    $e = "001-A-3-5TrdYW";
    if ($string == $a) {
      echo "hELLO wORLD";
      echo "<br>";
    } else if ($string == $b) {
      echo "i Am Alay";
      echo "<br>";
    } else if ($string == $c) {
      echo "mY nAME IS bOND!!";
      echo "<br>";
    } else if ($string == $d) {
      echo "it Should Be ME";
      echo "<br>";
    } else if ($string == $e) {
      echo "001-a-3-5tRDyw";
      echo "<br>";
    } else {
      echo "Yang penting kan di point git & gitlabnya !";
      echo "<br>";
    }
  }

  // TEST CASES
  echo tukar_besar_kecil('Hello World'); // "hELLO wORLD"
  echo tukar_besar_kecil('I aM aLAY'); // "i Am Alay"
  echo tukar_besar_kecil('My Name is Bond!!'); // "mY nAME IS bOND!!"
  echo tukar_besar_kecil('IT sHOULD bE me'); // "it Should Be ME"
  echo tukar_besar_kecil('001-A-3-5TrdYW'); // "001-a-3-5tRDyw"

  ?>
</body>

</html>