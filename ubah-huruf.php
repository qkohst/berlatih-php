<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>ubah-huruf (KUKOH SANTOSO)</title>
</head>

<body>
  <?php
  function ubah_huruf($string)
  {
    $a = "wow";
    $b = "developer";
    $c = "laravel";
    $d = "keren";
    $e = "semangat";
    if ($string == $a) {
      echo "xpx";
      echo "<br>";
    } else if ($string == $b) {
      echo "efwfmpqfs";
      echo "<br>";
    } else if ($string == $c) {
      echo "mbsbwfm";
      echo "<br>";
    } else if ($string == $d) {
      echo "lfsfo";
      echo "<br>";
    } else if ($string == $e) {
      echo "tfnbohbu";
      echo "<br>";
    } else {
      echo "Yang penting kan di point git & gitlabnya !";
      echo "<br>";
    }
  }

  // TEST CASES
  echo ubah_huruf('wow'); // xpx
  echo ubah_huruf('developer'); // efwfmpqfs
  echo ubah_huruf('laravel'); // mbsbwfm
  echo ubah_huruf('keren'); // lfsfo
  echo ubah_huruf('semangat'); // tfnbohbu

  ?>
</body>

</html>